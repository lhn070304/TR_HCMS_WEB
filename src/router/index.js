import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      name: 'Login',
      path: '/login',
      component: () => import("../views/Login.vue"),
      meta: { title: '登录页' }
    },
    {
      name: 'Home',
      path: '/home',
      component: () => import("../views/Home.vue"),
      meta: { 
        title: '首页',
        role: ['all']
      },
      children: [
        {
          path: '/fxyj',
          name: 'Fxyj',
          component: () => import('../components/fxyj/fxyj'),
          meta: { 
            title: '风险预警',
            role: ['all']
          }
        },
        {
          path: '/aqcn',
          name: 'Aqcn',
          component: () => import('../components/aqcn/aqcn'),
          meta: { 
            title: '安全承诺',
            role: ['all']
          }
        },
        {
          path: '/xcbg',
          name: 'Xcbg',
          component: () => import('../components/xcbg/xcbg'),
          meta: { 
            title: '巡查报告',
            role: ['all']
          }
        }
      ]
    },
    {
      name: '404',
      path: '/404',
      component: () => import('@/components/404.vue'),
      meta: { title: '路由不存在' }
    },
    {
      name: '403',
      path: '/403',
      component: () => import('@/components/403.vue'),
      meta: { title: '资源不可访问' }
    },
    {
      path: '*',
      redirect: '/404'
    }
  ],
  mode: 'history'
})
/**
 * 全局路由守卫
 */
const rightPathList = ['/login', '/404', '/403'];//直接可以进入的页面
router.beforeEach((to, from, next) => {
  var token = sessionStorage.getItem('user_token');
  if (!token && to.path != '/login') {//登陆认证校验,没登录则跳转/login
    next({ path: '/login' })
  }
  else {//权限认证
    if (rightPathList.includes(to.path)) {
      next();
    }
    else if (hasPermit(to)) {
      next();
    }
    else {
      next('403');
    }

  }
})
/**
 * 请求拦截器,添加请求头token
 */
axios.interceptors.request.use(
  config => {
    var headers = config.headers;
    if (sessionStorage.getItem("User-Token")) { //token
      // headers.authorization = 'Bearer '+sessionStorage.getItem("token"); token  jwt Bearer
      headers['App-Type'] = "PC-WEB";
      headers['Call-Mode'] = "HTTP";
      headers['User-Token'] = sessionStorage.getItem("User-Token");
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  });
//接口请求超时设置
axios.defaults.timeout=10000;//毫秒
/**
 * 应答拦截器,添加请求头token
 */
axios.interceptors.response.use(function (response) {
  return response;
}, error=> {
  return Promise.reject(error);
});


//获取当前路由是否有权限访问
function hasPermit(to) {
  console.log(to)
  var hasPermit = false;
  if (to.meta && to.meta.role) {
    var ruleList = getRuleList();
    hasPermit = ruleList.some(rule => {
      return to.meta.role.includes(rule);
    });
  }
  return hasPermit;

}
//获取登陆的角色集合
function getRuleList() {
  var ruleList = []; //角色数组
  var strRule = sessionStorage.getItem("authorities"); //authorities  position
  if (strRule.indexOf(",") != -1) {
    ruleList = strRule.split(",");
  } else {
    ruleList.push(strRule);
  }
  return ruleList;
}

export default router
