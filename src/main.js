import Vue from "vue"
import App from "./App.vue"
import store from "./store"

Vue.config.productionTip = false;

//1.引入axios
import axios from "axios";
Vue.prototype.$http = axios; //将axios改写到vue原型属性,调用方式例如this.$http.post(xxx)
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
axios.defaults.baseURL = 'http://dev_hcms.19871218.top/';

// axios.defaults.withCredentials = true;

//2.引入vuex
import Vuex from "vuex";
Vue.use(Vuex);

import "./assets/css/basic.scss";
//3.引入element
import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(Element);
//后引入路由,组件内样式可以覆盖elementui样式
import router from "./router";

//4.引入echarts
import myCharts from "./assets/js/myCharts.js";
Vue.use(myCharts);

//5.封装接口返回方法
import Ver from "./assets/js/Verification";
Vue.prototype.Ver = Ver;

//5.vue-echarts按需引入 
import ECharts from 'vue-echarts';
import 'echarts/lib/chart/bar' //柱状图
import 'echarts/lib/chart/line' //折线图
import 'echarts/lib/chart/pie' //饼状图
import 'echarts/lib/chart/radar' //雷达图
import 'echarts/lib/chart/tree' //树图
import 'echarts/lib/chart/pictorialBar' //象形柱图
import 'echarts/lib/chart/gauge' //仪表盘
import 'echarts/lib/component/polar' //极坐标
import 'echarts/lib/component/tooltip' //提示框
import 'echarts/lib/component/title' //title组件
import 'echarts/lib/component/legend' //图例
import 'echarts/lib/component/graphic' //自定义图形（圆心汉字...）
Vue.component('v-chart', ECharts)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
