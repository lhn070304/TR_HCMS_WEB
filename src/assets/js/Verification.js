export default function Ver(res) {
  if (res.data.error_code === '0') {
    return res.data.data;
  } else {
    return this.$notify.error({
      title: "错误",
      message: res.data.message,
      duration: 3000
    });
  }
}
//   中英文互换
// let storage=localStorage.getItem("key")
// if(storage=="zhCHS"){
//     this.ch=false
//     this.eng=true
//     this.$i18n.locale='zhCHS'
// }else {
//     this.$i18n.locale='en'
//     this.eng=false
//     this.ch=true
