/**
 * 各种画echarts图表的方法都封装在这里
 */
import echarts from "echarts/lib/echarts";
import "echarts/lib/chart/line"; //折线图
import "echarts/lib/chart/pie"; //饼状图
import "echarts/lib/component/tooltip"; //提示框
import "echarts/lib/component/title"; //title组件
import "echarts/lib/component/legend"; //图例
import "echarts/lib/component/graphic"; //自定义图形（圆心汉字...）

/**
 * 事例
 */
{
  /* <div id="chart1"></div>
this.$chart.line1('chart1'); */
}

const install = function(Vue) {
  Object.defineProperties(Vue.prototype, {
    $chart: {
      get() {
        return {
          //画一条简单的圆弧
          line1: function(id) {
            this.chart = echarts.init(document.getElementById(id));
            this.chart.clear();

            const optionData = {
              tooltip: {
                trigger: "item",
                formatter: "{a} <br/>{b}: {c} ({d}%)"
              },
              legend: {
                orient: "vertical",
                left: 10,
                data: [
                  "直接访问",
                  "邮件营销",
                  "联盟广告",
                  "视频广告",
                  "搜索引擎"
                ]
              },
              series: [
                {
                  name: "访问来源",
                  type: "pie",
                  radius: ["50%", "70%"],
                  avoidLabelOverlap: false,
                  label: {
                    show: false,
                    position: "center"
                  },
                  emphasis: {
                    label: {
                      show: true,
                      fontSize: "30",
                      fontWeight: "bold"
                    }
                  },
                  labelLine: {
                    show: false
                  },
                  data: [
                    { value: 335, name: "直接访问" },
                    { value: 310, name: "邮件营销" },
                    { value: 234, name: "联盟广告" },
                    { value: 135, name: "视频广告" },
                    { value: 1548, name: "搜索引擎" }
                  ]
                }
              ]
            };

            this.chart.setOption(optionData);
          }
        };
      }
    }
  });
};

export default {
  install
};
